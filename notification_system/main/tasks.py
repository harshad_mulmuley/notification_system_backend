from notification_system.celeryapp import app
from models import Notification
from datetime import datetime, timedelta
from time import sleep
from threading import Thread
from django.utils import timezone


@app.task()
def send_notifications():
    """
    This is periodic task which triggers every 15 minutes. For each notification
    present within 15 minutes of this task execution, we create new threads
    which sleep for a delta duration and then mark the send flag of a notif.
    :return:
    """
    notifs = Notification.objects.filter(
        notify_date__gt=timezone.now(),
        notify_date__lte=timezone.now()+timedelta(minutes=15),
        sent=False
    ).order_by('notify_date')

    for n in notifs:
        thread = create_new_thread(
            mark_notification,
            n,
            (n.notify_date-timezone.now()).seconds
        )
        thread.join()
    return "{0} notifications sent".format(len(notifs))


def mark_notification(notif, wait):
    for i in range(wait):
        sleep(1)
    notif.sent = True
    notif.save()


def create_new_thread(fun, notif, wait):
    thread = Thread(target=fun, args=(notif, wait))
    thread.start()
    return thread


@app.task()
def send_immediate_notification(notif):
    notif.sent = True
    notif.save()
    return "1 notification sent"
