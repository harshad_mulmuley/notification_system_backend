# Django
from django.shortcuts import render
from django.contrib.auth import logout
from django.template import RequestContext, loader
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.db import OperationalError
from django.contrib import messages
from django.utils import timezone
from tasks import send_immediate_notification
from django import forms

# Django REST Framework
from rest_framework import viewsets, mixins

# Models
from main.models import *
from main.serializers import SnippetSerializer
from main.forms import UserForm, NotificationForm


@login_required(login_url='/main/login/')
def index(request):
    """
    Lists all the notifications a user has received.
    """
    if request.method == 'GET':
        print "index: " + str(request.user)
        notifications = Notification.objects.filter(
            notify_to__id=request.user.id,
            sent=True
        ).order_by('-notify_date')
        context = {'notifications': notifications}
        return render(request, 'main/index.html', context)


@login_required(login_url='/main/login/')
def runsql(request):
    """
    View to run sql query to fetch user ids from user table. Only the queries
    on table auth_user can be run correctly.
    :param request:
    :return:
    """
    if request.method == 'GET':
        query = request.GET['query']
        if 'select' not in query.lower() or 'auth_user' not in query.lower():
            return JsonResponse({
                "status": "error",
                "data": "Select queries only on the"
                        " table auth_user are allowed."
            })
        ret_data = []
        try:
            for u in User.objects.raw(query):
                ret_data.append({'id':u.id, 'username':u.username})
        except OperationalError:
            return JsonResponse({
                "status": "error",
                 "data": "Somethings wrong with the SQL. Please try again. "
                         "Note that user table is auth_user "
                         "and user id field is id."
            })
        if len(ret_data) > 0:
            return JsonResponse({
                "status": "success",
                "data": ret_data ,
                "msg": "SQL executed successfully. "
                       "Users to be notified are now added "
                       "to the notify_to form field"
            })
        else:
            return JsonResponse({
                "status": "error",
                "data": "No User present for the given selection criterion."
            })


@login_required(login_url='/main/login/')
def dashboard(request):
    """

    :param request:
    :return:
    """
    if request.method == 'POST':
        notif_form = NotificationForm(data=request.POST)
        if notif_form.is_valid():
            receivers = request.POST.getlist('notify_to')
            if len(receivers) > 0:
                notification = notif_form.save(commit=False)
                notification.notify_by = request.user
                notification.save()
                for receiver in receivers:
                    notification.notify_to.add(receiver)
                notification.save()
                messages.success(request, "Notification scheduled successfully!")

                # If the notification is created within 15 minutes from now, we
                # schedule it immediately with seconds countdown.
                delay_seconds = (notification.notify_date - timezone.now()).seconds
                if 0 < delay_seconds <= 900:
                    send_immediate_notification.apply_async(
                        (notification,),
                        countdown=delay_seconds
                    )
            notif_form = NotificationForm()
        else:
            print notif_form.errors
        print request.POST
    else:
        notif_form = NotificationForm()
    return render(
        request,
        'main/dashboard.html',
        {'notif_form': notif_form}
    )


######################
# Registration Views #
######################

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
            return HttpResponseRedirect('/main/login/')
        else:
            print user_form.errors
    else:
        user_form = UserForm()


    return render(request,
            'main/register.html',
            {'user_form': user_form, 'registered': registered} )


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/main/dashboard/')
            else:
                return HttpResponse("Your account is disabled.")
        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

    else:
        return render(request, 'main/login.html', {})

def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/main/login/')


#########################
# Snippet RESTful Model #
#########################

class CRUDBaseView(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):
    pass

class SnippetView(CRUDBaseView):
    serializer_class = SnippetSerializer
    queryset = Snippet.objects.all()
