from django.core.exceptions import ValidationError


def validate_min_length(value):
    if len(value) < 20:
        raise ValidationError('Minimum char length should be 20.')
