$(document).ready(function () {
    $("#runsql").click(function (e) {
    	e.preventDefault();
    	var csrftoken = getCookie('csrftoken');
    	$.ajax(
    			{
    				url : "/main/runsql/",
    				type: "GET",
    				data: {
    					csrfmiddlewaretoken: csrftoken,
    					query: $('#query').val()
    				},
    				success: function(data){
    				    if(data.status == 'success'){
    				        $("#id_notify_to").html('');
    				        for(i in data.data){
    				            $("#id_notify_to").append(
    				                '<option value='+data.data[i].id+' selected="selected">'+data.data[i].username+'</option>'
    				            );
    				        }
                            $("#sqlerror").addClass("hidden");
    				        $("#sqlsuccess").removeClass("hidden");

                            $("#sqlsuccess").html(data.msg);
    				    }
    				    else{
    				        $("#sqlsuccess").addClass("hidden");
    				        $("#sqlerror").removeClass("hidden");
                            $("#sqlerror").html(data.data);
    				    }
    				    console.log(data)
    				},
    				error: function(xhr, errmsg, err){
    					console.log(xhr.status + ": "+ xhr.responseText);
    				}
    			}
    		);
    });
});

function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
		}
	}
 return cookieValue;
}