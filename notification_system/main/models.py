from django.db import models
from django.contrib.auth.models import User
from validators import validate_min_length


# Create your models here.
class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username


class Profile(models.Model):
    user = models.ForeignKey(User)
    oauth_token = models.CharField(max_length=200)
    oauth_secret = models.CharField(max_length=200)

    def __unicode__(self):
        return unicode(self.user)


class Notification(models.Model):
    notify_by = models.ForeignKey(User, related_name="notify_by")
    header = models.CharField(max_length=150, validators=[validate_min_length])
    content = models.CharField(max_length=300, validators=[validate_min_length])
    image_url = models.URLField()
    notify_date = models.DateTimeField()
    notify_to = models.ManyToManyField(User, related_name="notify_to")
    sent = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.header)


class Snippet(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    code = models.TextField()
    linenos = models.BooleanField(default=False)

    class Meta:
        ordering = ('created',)
