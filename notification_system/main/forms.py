from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from main.models import UserProfile, Notification
from validators import validate_min_length


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class NotificationForm(forms.ModelForm):
    notify_to = forms.CharField(required=True)
    header = forms.CharField(required=True, validators=[validate_min_length])
    content = forms.CharField(
        required=False,
        widget=forms.Textarea,
        validators=[validate_min_length]
    )
    image_url = forms.URLField(required=False)
    notify_date = forms.DateTimeField(
        required=True,
        widget=forms.DateTimeInput(),
        input_formats=['%d/%m/%Y %H:%M'],
        help_text="Please enter date time in this format: dd/mm/yyyy hh:mm"
    )

    class Meta:
        model = Notification
        fields = ('header', 'content', 'image_url', 'notify_to', 'notify_date')
