from django.contrib import admin
from main.models import UserProfile, Profile

admin.site.register(UserProfile)
admin.site.register(Profile)