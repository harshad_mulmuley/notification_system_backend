Django Notification Creation Dashboard
--------------------------------------
Getting Started
---------------
To get up and running, simply do the following:

    $ git clone

    # Install the requirements (recommended in virtualenv)
    $ pip install -r requirements.txt

    # Install bower
    $ npm install -g bower
    $ bower install

    $ cd notification_system
    
    # Perform database migrations
    $ python manage.py makemigrations
    $ python manage.py migrate

    # Runserver
    $ python manage.py runserver

    # Install rabbitMQ
    $ brew install rabbitmq

    # Run Celery Worker
    celery -A notification_system worker -B --loglevel=info


Navigation
----------
1. Once the server is up, navigate to http://127.0.0.1:8000/main/register/ to register a new user.
2. Next, http://127.0.0.1:8000/main/login/ to login
3. Once logged in, notification creation dashboard is available at http://127.0.0.1:8000/main/dashboard/
4. User notifications can be seen at http://127.0.0.1:8000/

Functionalities
---------------
* Notification dashboard takes following inputs:

    1. SQL to select users to whom notification can be sent.
    2. Notification header
    3. Notification content
    4. Image url
    5. Notification sending date time in format `dd/mm/yyyy hh:mm`

* A user can create new notifications on the dashboard and assign them to other users setting any future date and time.
* A periodic celery task is setup which triggers every 15 minutes and process all the future notifications within next 15 minutes.
* Each notification will be delivered (marked as sent) at the provided notification date.
* Notification list view is also provided.

Limitations
-----------
* Date time has to be text entered according to the specified format. Datepicker is not added.
* New notifications can be viewed only after refreshing the index page.

Credits
-------
Django starter kit is used from https://github.com/DrkSephy/django-hackathon-starter


